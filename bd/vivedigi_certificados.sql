-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 08-06-2016 a las 15:04:43
-- Versión del servidor: 5.5.49-cll
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `vivedigi_certificados`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `certificados_pvd`
--

CREATE TABLE IF NOT EXISTS `certificados_pvd` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `n_registro` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_expedicion` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `intensidad_horaria` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_certificado` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `certificados_pvd`
--

INSERT INTO `certificados_pvd` (`id`, `n_registro`, `fecha_expedicion`, `intensidad_horaria`, `nombre_certificado`) VALUES
(1, '1114062253', '20 MAYO 2016', '40', 'Apropiación TIC'),
(2, '1114062253', '22 ABRIL 2016', '90', 'Periodismo Digital');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE IF NOT EXISTS `cursos` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `nombre_curso` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inscripcion` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id`, `nombre_curso`, `fecha_inscripcion`) VALUES
(1, 'Apropiacion Tic', '2016-05-31 09:59:21'),
(2, 'Periodismo digital', '2016-05-31 09:59:49'),
(3, 'hola', '2016-05-31 16:02:38'),
(4, 'dfdsfsdfa', '2016-05-31 16:30:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `friend_count` int(11) DEFAULT NULL,
  `profile_pic` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`uid`, `username`, `password`, `email`, `friend_count`, `profile_pic`) VALUES
(1, 'tecnotaxia', 'e8adc509782d97fcc4a2f88b93f63d86', 'yvallejo@tecnotaxia.com', NULL, NULL),
(2, 'vivedigital', 'hola123456', 'druiz@tecnotaxia.com', NULL, NULL),
(3, 'pvd', '32e4c07203b9aaf2e61b2a59ade4649a', 'druiz@tecnotaxia.com', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_identificacion` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `n_identificacion` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombres`, `apellidos`, `tipo_identificacion`, `n_identificacion`) VALUES
(1, 'DUBERNEY', 'RUIZ BEDOYA', 'CEDULA', '1114062253');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
