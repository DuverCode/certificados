<!doctype html>

<html>

<head>

<meta charset="utf-8">

<title>Certificados - Vive Digital Tuluá</title>

<link href="css/estilos.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<section class="main">

<div id="global-containerUno" class="global-headerUno">

        <header id="main-headerUno">

            <figure class="header-logoUno"><img src="http://vivedigitaltulua.gov.co/beta/wp-content/uploads/2016/04/Logo-puntos-vive-digital_03.png" alt=""></figure>

        </header><!-- /#main-header -->

        </div>
<span class="icono-certificadosUno"></span>


  <div class="div_form">
  <div class="personaUno"></div>
  <div class="certificadoUno">
    <p align="center" class="text-certificado">Debe diligenciar el     número de su documento de identificación.<br>
    Esta opción le permite obtener una lista de los certificados que tiene disponibles para descarga. Utilice el botón <b>Consultar</b> que se halla en la sección correspondiente para obtener o validar la existencia de un certificado.
        </p>  

    	<form name="form" action="" method="POST" id="search_form">

        	<table width="100%" border="0" align="center" cellpadding="5">

            	<tr>

                	<tr>
<!--
                    <td align="right" width="30%">
                    <span style="font-weight:bold; width:100%"></span>
                    </td>
-->
                    <td align="center" width="100%">
                    <input type="text" name="n_identificacion" id="n_identificacion" class="input_text" />
                    </td>
                </tr>
                <td align="center" width="100%">
                    <span class="btn-consultar"><input type="submit" name="Consultar" id="consultar" value="Consultar" class="input_button btn-consultar" /></span>
                    <input type="hidden" name="generar" value="si"   />
                    </td>

            </table>

        </form>

        </div>
<div class="marco">
        <div class="resultado" id="resultado">

    	<form name="GenerarPDF" method="get" action="convertirPDF.php" target="_blank">

            <table width="100%" border="0" align="center" cellpadding="5">

                <thead>

                    <tr>

                        <th width="10%" align="center">Cedula</th>

                        <th width="25%" align="center">Nombres</th>

                        <th width="25%" align="center">Apellidos</th>

                        <th width="25%" align="center">Taller / Capacitación</th>

                        <th width="35%" align="center">Descargar certificado</th>


                    </tr>

                </thead>

                <tbody class="tbody">

                <?php 

					if(isset($_POST["generar"]) and $_POST["generar"]=="si"){
                        require_once("class/consultar.php");

                        echo "<style>.resultado{display:block;}</style>";

						$obj= new  Consultar();

						$resultado= $obj->get_estudiantes($_POST["n_identificacion"]);

						if($resultado!="NULL"){	

							for($i=0; $i<sizeof($resultado); $i++){?>
                                <input 

                                type="hidden"

                                name="tipo_identificacion" 

                                value="<?php echo $resultado[$i]["tipo_identificacion"];?>"

                                class="text_hidden" 


                                readonly

                                />

                               <input 

                                type="hidden"

                                name="intensidad_horaria" 

                                value="<?php echo $resultado[$i]["intensidad_horaria"];?>"

                                class="text_hidden" 


                                readonly

                                />

                                
                                 <input 

                                type="hidden"

                                name="fecha_expedicion" 

                                value="<?php echo $resultado[$i]["fecha_expedicion"];?>"

                                class="text_hidden" 


                                readonly

                                />

                                <input 

                                type="hidden" 

                                value="<?php echo $resultado[$i]["id"];?>"

                                class="text_hidden"

                                readonly

                                />


							<tr>


                                <td align="center">

								<input 

                                type="text"

                                name="n_identificacion" 

                                class="text_hidden" 

                                value="<?php echo $resultado[$i]["n_identificacion"];?>"

                                readonly

                                />

								</td>

								<td align="center">

								<input 

                                type="text" 

                                name="nombres" 

                                value="<?php echo $resultado[$i]["nombres"];?>"

                                class="text_hidden"

                                readonly

                                />

								</td>

								<td align="center">

								<input 

                                type="text" 

                                name="apellidos"

                                value="<?php echo $resultado[$i]["apellidos"];?>"

                                class="text_hidden"

                                readonly

                                />

								</td>

								<td align="center">

								<input 

                                type="text" 

                                name="nombre_certificado"

                                value="<?php echo $resultado[$i]["nombre_certificado"];?>"

                                class="text_hidden"

                                readonly

                                />

								</td>

								<td align="center">

								
                                

                                <a class="btn-descargar" target="_blank" href="convertirPDF.php?n_identificacion=<?php echo $resultado[$i]["n_identificacion"];?>&nombre_certificado=<?php echo $resultado[$i]["nombre_certificado"];?>&nombres=<?php echo $resultado[$i]["nombres"];?>&apellidos=<?php echo $resultado[$i]["apellidos"];?>&fecha_expedicion=<?php echo $resultado[$i]["fecha_expedicion"];?>&intensidad_horaria=<?php echo $resultado[$i]["intensidad_horaria"];?>&intensidad_horaria=<?php echo $resultado[$i]["intensidad_horaria"];?>&tipo_identificacion=<?php echo $resultado[$i]["tipo_identificacion"];?>">Descargar</a>
                                

								</td>

							</tr>

				<?php  

							}//fin del FOR

						}//fin del if($resultado!="NULL"){

							else{?>

									<tr>

                                    	<td colspan="5" align="center"><span style="font-weight:bold; color:rgba(255,0,0,1); font-size:20px;">No Existe El Registro Por Favor Verifique Los Datos</span><hr></td>

                                    	

                                    <tr>

							<?php }

					}//Fin del if(isset($_POST["generar"]) and $_POST["generar"]=="si")

				?>

                </tbody>	

            </table>

		</form>   
</div>
</div>
  </div>

</section>

</body>

</html>